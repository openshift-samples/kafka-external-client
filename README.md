# Kafka External Client

Example on how to access Kafka deployed in OpenShift.

## Requirements

- Fuse 7.9
- Kafka Operator 1.8.0
- OpenShift 4.8 or above

## Running the example

If you need to delete your previous cluster

    oc delete kafka ${cluster_name}

### Create project

Declare the *PROJECT* variable with the project name and then create the project

    export PROJECT=
    oc new-project $PROJECT

### Install the operator

    oc apply -f openshift/amq-streams-operatorgroup.yml -n $PROJECT
    oc apply -f openshift/amq-streams-subscription.yml -n $PROJECT    

### Create the cluster instance

    oc apply -f openshift/kafka-ephemeral.yaml -n $PROJECT

### Create the topic and user

    oc apply -f openshift/kafka-topic.yaml -n $PROJECT
    oc apply -f openshift/kafka-user.yaml   -n $PROJECT

### Update apps configuration

Before running the apps you must:

1. update producer's [application-example.properties](producer/src/main/resources/application-example.properties) and consumer's [application-example.properties](consumer/src/main/resources/application-example.properties) to reflect your environment by changing:
  - The routes
  - User password
  - Truststore location

  Then rename `application-example.properties` to `application.properties` .

2. You also need to Update the truststore:

        cd truststore/
        ./create-new-truststore.sh


## How to run the apps?

    mvn clean spring-boot:run    

## Using Kafka scripts

### To read from Kafka

by ` oc rsh ` in one Kafka's cluster member you are able to use Kafka scripts

    /opt/kafka/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --from-beginning
