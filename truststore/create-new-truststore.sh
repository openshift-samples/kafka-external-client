#!/bin/sh

if [ -f "client-truststore.jks" ]; then
    rm client-truststore.jks
fi

oc extract -n $PROJECT secret/my-cluster-cluster-ca-cert --keys ca.crt

keytool -keystore client-truststore.jks -alias CARoot -import -file ca.crt

if [ -f "ca.crt" ]; then
    rm ca.crt
fi
