package com.streams.client.external;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class CamelRouter extends RouteBuilder {

  private int messageCounter;

    @Override
    public void configure() throws Exception {

        from("timer://myTimer")
            .routeId("message-sender")
                .process(exchange -> {
                    messageCounter++;
                    exchange.getIn().setBody("message number ("+ messageCounter + ")");
                })
            .to("{{kafka.producer.url}}")
            .log("message sent");

    }

}
