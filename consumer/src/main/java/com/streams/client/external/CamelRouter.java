package com.streams.client.external;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class CamelRouter extends RouteBuilder {

    int messageCounter = 0;

    @Override
    public void configure() throws Exception {

        from("{{kafka.consumer.url")
            .routeId("message-receiver")
            .log("Received message: ${body}");

    }

}
